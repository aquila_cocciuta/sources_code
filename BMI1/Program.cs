﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BMI1
{
    class Program
    {

        private static void Main(string[] args)
        {
            double BMI;
            string Classificazione = "";
            List<BMIResult> ResultObjList = new List<BMIResult>();

            foreach (string l in File.ReadAllLines(@"C:\Users\Daria\Desktop\c#\MySimpleServiceCSV\input\dati.csv"))
            {
                string[] values = l.Split(';');
                BMI = (Convert.ToInt32(values[1])) / (Math.Pow(Convert.ToDouble(values[2]), 2));

                if (BMI < 18.5)
                {
                    Classificazione = "SOTTOPESO";
                }
                else if (BMI >= 18.5 || BMI < 24.9)
                {
                    Classificazione = "NORMALE";
                }
                else if (BMI >= 25 || BMI < 29)
                {
                    Classificazione = "SOVRAPPESO";
                }
                else if (BMI >= 30)
                {
                    Classificazione = "OBESO";
                }
                ResultObjList.Add(new BMIResult(values[0], Convert.ToInt32(values[1]), Convert.ToDouble(values[2]), Classificazione));
            }


            StreamWriter writer = new StreamWriter(@"C:\Users\Daria\Desktop\c#\MySimpleServiceCSV\output\out\output.csv");

                foreach (BMIResult e in ResultObjList)
                {
                 writer.WriteLine(e.Nome + "; " + e.Peso + ";" + e.Classificazione);
                //Console.WriteLine(e.Nome + ";" + e.Peso + ";" + e.Classificazione);
                //Console.ReadLine();
                
                }

                writer.Dispose();
                
        }
    }
}

