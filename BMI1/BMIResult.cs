﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMI1
{
    class BMIResult
    {   
        public BMIResult(string nome, int peso, double altezza, string classificazione)
        {
            Nome = nome;
            Peso = peso;
            Altezza = altezza;
            Classificazione=classificazione;
        }
        public string Nome
        {
             get;
             set;
        }
        public int Peso
        {
            get;
            set;
        }

        public double Altezza
        {
            get;
            set;
        }
        public string Classificazione
        {
            get;
            set;
        }


    }
}
