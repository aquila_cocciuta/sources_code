﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMI_LeggiElaboraScrivi
{
    class Scrivi
    {
        public string path { get; set; }
        public string nomeOutput { get; set; }
        public Scrivi(string path, string nomeOutput)
        {
            this.path = path;
            this.nomeOutput = nomeOutput;
        }
        public void Scrittura(List<string> ResultList)
        {
            StreamWriter writer = new StreamWriter(path+nomeOutput);

            foreach (string l in ResultList)
            {
                writer.WriteLine(l);
                //Console.WriteLine(e.Nome + ";" + e.Peso + ";" + e.Classificazione);
                //Console.ReadLine();

            }

            writer.Dispose();
        }
    }
}
