﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BMI_LeggiElaboraScrivi
{
    class Elabora
    {
        public string nome { get; set; }
        public int peso { get; set; }
        public double altezza { get; set; }
        public double bmi { get; set; }
        public string classificazione { get; set; }
        public List<string> risultatoElaborazione = new List<string>();
        public Elabora()
        {
        }

        public List<string> Elaborazione(List<string> ResultList)
        {
            foreach(string l in ResultList)
            {
                for (int i = 0;i< ResultList.Count;i++)
                {
                    string[] values = l.Split(';');
                    nome = values[0];
                    peso = Convert.ToInt32(values[1]);
                    altezza= Convert.ToDouble(values[2]);
                    bmi= peso / (Math.Pow(altezza, 2));

                    if (bmi < 18.5)
                    {
                        classificazione = "SOTTOPESO";
                    }
                    else if (bmi >= 18.5 || bmi < 24.9)
                    {
                        classificazione = "NORMALE";
                    }
                    else if (bmi >= 25 || bmi < 29)
                    {
                        classificazione = "SOVRAPPESO";
                    }
                    else if (bmi >= 30)
                    {
                        classificazione = "OBESO";
                    }            
                }
                risultatoElaborazione.Add(nome + ';' + (Convert.ToString(peso)) + ';' + classificazione);
            }
            return risultatoElaborazione;
        }
    }
}
