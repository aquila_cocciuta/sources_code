﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMI_LeggiElaboraScrivi
{
    class Leggi
    {
        public string path { get; set; }
        public string nome { get; set; }
        public int peso { get; set; }
        public double altezza { get; set; }
        public string classificazione { get; set; }

        public Leggi(string BasePath, string nomeFile)
        {
            this.path = BasePath + nomeFile;
        }

        public List<string> Lettura()
        {
            List<string> ResultList = new List<string>();
            foreach (string l in File.ReadAllLines(path))
            {
                ResultList.Add(l); 
            }
            return ResultList;
        }
    }
}
