﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMI_LeggiElaboraScrivi
{
    class Program
    {
        private static void Main(string[] args)
        {
            string BasePath = @"C:\Users\Daria\Desktop\c#\MySimpleServiceCSV\";
            var lettore = new Leggi(BasePath, "Input.csv");
            var elaboratore = new Elabora();
            var scrittore = new Scrivi(BasePath,"Output.csv");
            scrittore.Scrittura(elaboratore.Elaborazione(lettore.Lettura()));

        }
    }
}
